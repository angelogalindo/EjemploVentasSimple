﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="RealizarVenta.aspx.vb" Inherits="Operacion_RealizarVenta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <h1 class="page-header">
        Realizar Venta  
        <br />   
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>         
        <asp:HiddenField ID="hdnEstado" runat="server" />
    </h1> 
     <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"> 
            <label>Serie</label>
            <asp:TextBox ID="txtSerie" ReadOnly="true" CssClass="form-control" Text="A" runat="server"></asp:TextBox>
            <label>Numero</label>
            <asp:TextBox ID="txtNumero" CssClass="form-control" ReadOnly="true" runat="server"></asp:TextBox>    
            <br />
            
            <asp:Button ID="cmdGuardarEncabezado" CssClass="btn btn-primary"  runat="server" Text="Generar Factura" />
            <asp:Button ID="cmdFinalizar" CssClass="btn btn-warning"  runat="server" Text="Finalizar Venta" />        
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"> 
             <label>Cliente</label>            
            <asp:DropDownList ID="cboCliente" CssClass="form-control" runat="server" DataSourceID="sqlCliente" DataTextField="NOMBRE_CLIENTE" DataValueField="CODIGO_CLIENTE"></asp:DropDownList>
            <asp:SqlDataSource runat="server" ID="sqlCliente" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT 0 as CODIGO_CLIENTE, 'Seleccione cliente' NOMBRE_CLIENTE
UNION
SELECT CODIGO_CLIENTE,NOMBRE_CLIENTE FROM clientes"></asp:SqlDataSource>
             <label>Fecha</label>
            <asp:TextBox ID="txtFecha" CssClass="form-control" ReadOnly="true" runat="server"></asp:TextBox>
             <br />            
            
         </div>
         

    </div>
    <hr />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

        
        <div class="row">
            <div class="col-lg-2">
                <label>Codigo del producto</label>
                <div class="form-group input-group"> 
                    <asp:TextBox ID="txtCodigoProducto"  CssClass="form-control"  runat="server"></asp:TextBox>    
                    <span class="input-group-btn">
                        <asp:Button ID="cmdBuscar" CssClass="btn btn-default"  runat="server" Text="Buscar" />        
                    </span>
                </div>
            
            
            </div>
            <div class="col-lg-4">
                <label>Producto</label>
                <asp:TextBox ID="txtDescripcion" ReadOnly="true" CssClass="form-control"  runat="server"></asp:TextBox>    
            </div>
            <div class="col-lg-2">
                <label>Cantidad</label>
                <asp:TextBox ID="txtCantidad" Width="150px" CssClass="form-control"  runat="server"></asp:TextBox>    
            </div>
            <div class="col-lg-2">
                <label>Precio</label>
                <asp:TextBox ID="txtPrecio" Width="150px" CssClass="form-control"  runat="server"></asp:TextBox>    
            </div>
            <div class="col-lg-2">  
                <br />
                <asp:Button ID="cmdAgregarProducto" style="vertical-align:bottom" CssClass="btn btn-primary"  runat="server" Text="Agregar" />
            </div>
        </div>
    
        <div class="row">
            <div class="col-lg-12">
                    <br />
                    <div class="row">
                        <div class="col-lg-9">
                        </div>
                        <div class="col-lg-3">
                            <label  style="display:inline-block">Total</label>
                            <asp:TextBox ID="txtTotal" Width="200px" style="display:inline-block; text-align:right" Text="0.00" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>                        
                    
                    <telerik:RadGrid ID="grdDetalle" runat="server" DataSourceID="sqlDetalleFactura" >
                        <MasterTableView DataKeyNames="CORRELATIVO,CODIGO_PRODUCTO" DataSourceID="sqlDetalleFactura" AutoGenerateColumns="False">
                            <Columns>
                                <telerik:GridBoundColumn DataField="CORRELATIVO" ReadOnly="True" HeaderText="CORRELATIVO" SortExpression="CORRELATIVO" UniqueName="CORRELATIVO" DataType="System.Int32" FilterControlAltText="Filter CORRELATIVO column" Display="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="SERIE" HeaderText="SERIE" SortExpression="SERIE" UniqueName="SERIE" FilterControlAltText="Filter SERIE column" Display="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NUMERO" HeaderText="NUMERO" SortExpression="NUMERO" UniqueName="NUMERO" DataType="System.Int32" FilterControlAltText="Filter NUMERO column" Display="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CODIGO_PRODUCTO" ReadOnly="True" HeaderText="CODIGO_PRODUCTO" SortExpression="CODIGO_PRODUCTO" UniqueName="CODIGO_PRODUCTO" DataType="System.Int32" FilterControlAltText="Filter CODIGO_PRODUCTO column" Display="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="DESCRIPCION" HeaderText="DESCRIPCION" SortExpression="DESCRIPCION" UniqueName="DESCRIPCION" FilterControlAltText="Filter DESCRIPCION column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CANTIDAD" HeaderText="CANTIDAD" SortExpression="CANTIDAD" UniqueName="CANTIDAD" DataType="System.Int32" FilterControlAltText="Filter CANTIDAD column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PRECIO" HeaderText="PRECIO" SortExpression="PRECIO" UniqueName="PRECIO" DataType="System.Decimal" FilterControlAltText="Filter PRECIO column"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="SUBTOTAL" HeaderText="Subtotal" SortExpression="SUBTOTAL" UniqueName="SUBTOTAL" DataType="System.Decimal" FilterControlAltText="Filter SUBTOTAL column"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>

                    <asp:SqlDataSource runat="server" ID="sqlDetalleFactura" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT CORRELATIVO, SERIE, NUMERO, CODIGO_PRODUCTO, DESCRIPCION, CANTIDAD, PRECIO, SUBTOTAL FROM qrydetallefactura WHERE (SERIE = @Serie) AND (NUMERO = @Numero)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtSerie" Type="String"  Name="@Serie" />
                            <asp:ControlParameter ControlID="txtNumero" Type="Int32"  Name="@Numero" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

