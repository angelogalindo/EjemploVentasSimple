﻿
Partial Class Operacion_RealizarVenta
    Inherits System.Web.UI.Page

    Private Parametros As MySqlParameterCollection = New MySqlCommand().Parameters
    Private WithEvents cls As New ControlDeDatos()
    Enum EstadoFactura
        SinEstado
        AgregandoProductos
    End Enum

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then

            txtFecha.Text = Date.Now.ToShortDateString
            hdnEstado.Value = EstadoFactura.SinEstado
            Estados()
        End If
    End Sub

    Protected Sub cls_sqlError(DescripcionError As String) Handles cls.sqlError
        lblMensaje.Text = DescripcionError
    End Sub

    Private Sub Estados()
        If hdnEstado.Value = EstadoFactura.SinEstado Then
            cboCliente.Enabled = True
            cmdGuardarEncabezado.Enabled = True
            cmdFinalizar.Enabled = False
            cmdAgregarProducto.Enabled = False
        ElseIf hdnEstado.Value = EstadoFactura.AgregandoProductos Then
            cboCliente.Enabled = False
            cmdGuardarEncabezado.Enabled = False
            cmdFinalizar.Enabled = True
            cmdAgregarProducto.Enabled = True

        End If
    End Sub

    Protected Sub cmdGuardarEncabezado_Click(sender As Object, e As EventArgs) Handles cmdGuardarEncabezado.Click
        If Page.IsValid Then
            lblMensaje.Text = ""
            If cboCliente.SelectedValue > 0 Then
                Dim Numero As Integer = cls.Max("factura", "NUMERO")
                Parametros.Add("@SERIE", MySqlDbType.VarChar).Value = txtSerie.Text
                Parametros.Add("@NUMERO", MySqlDbType.Int24).Value = Numero
                Parametros.Add("@CODIGO_CLIENTE", MySqlDbType.Int24).Value = cboCliente.SelectedValue
                Parametros.Add("@FECHA", MySqlDbType.Date).Value = Format(CDate(txtFecha.Text), "yyyy-MM-dd")

                If cls.Insertar("factura", Parametros) Then
                    txtNumero.Text = Numero
                    hdnEstado.Value = EstadoFactura.AgregandoProductos
                    Estados()
                End If
            Else
                lblMensaje.Text = "Seleccione cliente"
            End If
        End If

    End Sub

    Protected Sub cmdFinalizar_Click(sender As Object, e As EventArgs) Handles cmdFinalizar.Click
        txtNumero.Text = ""
        cboCliente.SelectedValue = 0
        txtFecha.Text = Date.Now.ToShortDateString
        txtTotal.Text = "0.00"
        hdnEstado.Value = EstadoFactura.SinEstado
        Estados()
    End Sub

    Protected Sub cmdAgregarProducto_Click(sender As Object, e As EventArgs) Handles cmdAgregarProducto.Click
        If Page.IsValid Then
            lblMensaje.Text = ""
            If txtCodigoProducto.Text <> "" And txtCantidad.Text <> "" And txtPrecio.Text <> "" Then
                Parametros.Add("@CORRELATIVO", MySqlDbType.Int24).Value = cls.Max("detalle", "CORRELATIVO")
                Parametros.Add("@SERIE", MySqlDbType.VarChar).Value = txtSerie.Text
                Parametros.Add("@NUMERO", MySqlDbType.VarChar).Value = txtNumero.Text
                Parametros.Add("@CODIGO_PRODUCTO", MySqlDbType.Int24).Value = txtCodigoProducto.Text
                Parametros.Add("@CANTIDAD", MySqlDbType.Int24).Value = txtCantidad.Text
                Parametros.Add("@PRECIO", MySqlDbType.Decimal).Value = txtPrecio.Text

                If cls.Insertar("detalle", Parametros) Then
                    txtCodigoProducto.Text = ""
                    txtDescripcion.Text = ""
                    txtCantidad.Text = ""
                    txtPrecio.Text = ""
                    Parametros.Clear()
                    Parametros.Add("@SERIE", MySqlDbType.VarChar).Value = txtSerie.Text
                    Parametros.Add("@NUMERO", MySqlDbType.VarChar).Value = txtNumero.Text
                    txtTotal.Text = cls.RetornarValor("qryDetalleFactura", "SUM(SubTotal)", Parametros)
                    grdDetalle.Rebind()
                Else
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "msjProducto", "alert('" & cls.MensajeError & "');", True)
                End If


            End If
        End If
    End Sub

    Protected Sub cmdBuscar_Click(sender As Object, e As EventArgs) Handles cmdBuscar.Click
        If txtCodigoProducto.Text <> "" Then
            Parametros.Add("@CODIGO_PRODUCTO", MySqlDbType.Int24).Value = txtCodigoProducto.Text
            txtDescripcion.Text = cls.RetornarValor("productos", "DESCRIPCION", Parametros)
            If txtDescripcion.Text = "" Then
                txtCodigoProducto.Text = ""
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "msjProducto", "alert('Producto no encontrado');", True)
            Else
                txtCantidad.Focus()
            End If
        Else
            txtCodigoProducto.Text = ""
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "msjProducto", "alert('Debe ingresar un codigo');", True)
        End If

        
    End Sub
End Class
