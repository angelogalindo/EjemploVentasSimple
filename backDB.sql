-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.6.17 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para facturacion
CREATE DATABASE IF NOT EXISTS `facturacion` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `facturacion`;


-- Volcando estructura para tabla facturacion.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `CODIGO_CLIENTE` int(11) NOT NULL,
  `NOMBRE_CLIENTE` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `CODIGO_VENDEDOR` int(11) NOT NULL,
  PRIMARY KEY (`CODIGO_CLIENTE`),
  KEY `FK_VENDEDORES_CLIENTES` (`CODIGO_VENDEDOR`),
  CONSTRAINT `FK_VENDEDORES_CLIENTES` FOREIGN KEY (`CODIGO_VENDEDOR`) REFERENCES `vendedores` (`CODIGO_VENDEDOR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla facturacion.clientes: ~2 rows (aproximadamente)
DELETE FROM `clientes`;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`CODIGO_CLIENTE`, `NOMBRE_CLIENTE`, `CODIGO_VENDEDOR`) VALUES
	(1, 'Esteban Ramos', 3),
	(2, 'Juan Garcia', 1),
	(3, 'Jorge ', 3);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;


-- Volcando estructura para tabla facturacion.detalle
CREATE TABLE IF NOT EXISTS `detalle` (
  `CORRELATIVO` int(11) NOT NULL,
  `SERIE` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `NUMERO` int(11) NOT NULL,
  `CODIGO_PRODUCTO` int(11) NOT NULL,
  `CANTIDAD` int(11) DEFAULT NULL,
  `PRECIO` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`CORRELATIVO`),
  KEY `FK_FK_FACTURA_DETALLE` (`SERIE`,`NUMERO`),
  KEY `FK_FK_PRODUCTOS_DETALLE` (`CODIGO_PRODUCTO`),
  CONSTRAINT `FK_FK_PRODUCTOS_DETALLE` FOREIGN KEY (`CODIGO_PRODUCTO`) REFERENCES `productos` (`CODIGO_PRODUCTO`),
  CONSTRAINT `FK_FK_FACTURA_DETALLE` FOREIGN KEY (`SERIE`, `NUMERO`) REFERENCES `factura` (`SERIE`, `NUMERO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla facturacion.detalle: ~11 rows (aproximadamente)
DELETE FROM `detalle`;
/*!40000 ALTER TABLE `detalle` DISABLE KEYS */;
INSERT INTO `detalle` (`CORRELATIVO`, `SERIE`, `NUMERO`, `CODIGO_PRODUCTO`, `CANTIDAD`, `PRECIO`) VALUES
	(1, 'A', 1, 1, 2, 2.50),
	(2, 'A', 1, 1, 2, 2.50),
	(3, 'A', 1, 2, 3, 1.00),
	(4, 'A', 1, 1, 2, 3.00),
	(5, 'A', 2, 2, 10, 2.25),
	(6, 'A', 3, 1, 5, 5.00),
	(7, 'A', 3, 2, 6, 2.60),
	(8, 'A', 3, 1, 6, 2.60),
	(9, 'A', 4, 1, 5, 6.00),
	(10, 'A', 4, 2, 10, 2.25),
	(11, 'A', 4, 1, 6, 25.00),
	(12, 'A', 5, 1, 2, 45.00),
	(13, 'A', 5, 2, 5, 18.00),
	(14, 'A', 5, 1, 6, 0.36),
	(15, 'A', 6, 3, 45, 5.00),
	(16, 'A', 6, 1, 4, 2.25);
/*!40000 ALTER TABLE `detalle` ENABLE KEYS */;


-- Volcando estructura para tabla facturacion.factura
CREATE TABLE IF NOT EXISTS `factura` (
  `SERIE` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `NUMERO` int(11) NOT NULL,
  `CODIGO_CLIENTE` int(11) NOT NULL,
  `FECHA` date DEFAULT NULL,
  PRIMARY KEY (`SERIE`,`NUMERO`),
  KEY `FK_FK_CLIENTE_FACTURA` (`CODIGO_CLIENTE`),
  CONSTRAINT `FK_FK_CLIENTE_FACTURA` FOREIGN KEY (`CODIGO_CLIENTE`) REFERENCES `clientes` (`CODIGO_CLIENTE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla facturacion.factura: ~4 rows (aproximadamente)
DELETE FROM `factura`;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` (`SERIE`, `NUMERO`, `CODIGO_CLIENTE`, `FECHA`) VALUES
	('A', 1, 1, '2015-10-09'),
	('A', 2, 2, '2015-10-09'),
	('A', 3, 1, '2015-10-09'),
	('A', 4, 1, '2015-10-09'),
	('A', 5, 1, '2015-10-09'),
	('A', 6, 1, '2015-10-09');
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;


-- Volcando estructura para tabla facturacion.productos
CREATE TABLE IF NOT EXISTS `productos` (
  `CODIGO_PRODUCTO` int(11) NOT NULL,
  `DESCRIPCION` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PRODUCTO_PADRE` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODIGO_PRODUCTO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla facturacion.productos: ~2 rows (aproximadamente)
DELETE FROM `productos`;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` (`CODIGO_PRODUCTO`, `DESCRIPCION`, `PRODUCTO_PADRE`) VALUES
	(1, 'Mouse', 2),
	(2, 'Cable UPT', 1),
	(3, 'Teclado USB', 1);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;


-- Volcando estructura para vista facturacion.qryclientes
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `qryclientes` (
	`CODIGO_CLIENTE` INT(11) NOT NULL,
	`NOMBRE_CLIENTE` VARCHAR(15) NULL COLLATE 'utf8_spanish_ci',
	`CODIGO_VENDEDOR` INT(11) NOT NULL,
	`NOMBRE_VENDEDOR` VARCHAR(15) NULL COLLATE 'utf8_spanish_ci'
) ENGINE=MyISAM;


-- Volcando estructura para vista facturacion.qrydetallefactura
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `qrydetallefactura` (
	`CORRELATIVO` INT(11) NOT NULL,
	`SERIE` VARCHAR(1) NOT NULL COLLATE 'utf8_spanish_ci',
	`NUMERO` INT(11) NOT NULL,
	`CODIGO_PRODUCTO` INT(11) NOT NULL,
	`DESCRIPCION` VARCHAR(15) NULL COLLATE 'utf8_spanish_ci',
	`CANTIDAD` INT(11) NULL,
	`PRECIO` DECIMAL(10,2) NULL,
	`SUBTOTAL` DECIMAL(20,2) NULL
) ENGINE=MyISAM;


-- Volcando estructura para vista facturacion.qryresumenfacturas
-- Creando tabla temporal para superar errores de dependencia de VIEW
CREATE TABLE `qryresumenfacturas` (
	`Serie` VARCHAR(1) NOT NULL COLLATE 'utf8_spanish_ci',
	`Numero` INT(11) NOT NULL,
	`Fecha` DATE NULL,
	`Cliente` VARCHAR(15) NULL COLLATE 'utf8_spanish_ci',
	`Total` DECIMAL(42,2) NULL
) ENGINE=MyISAM;


-- Volcando estructura para tabla facturacion.vendedores
CREATE TABLE IF NOT EXISTS `vendedores` (
  `CODIGO_VENDEDOR` int(11) NOT NULL,
  `NOMBRE_VENDEDOR` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`CODIGO_VENDEDOR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla facturacion.vendedores: ~3 rows (aproximadamente)
DELETE FROM `vendedores`;
/*!40000 ALTER TABLE `vendedores` DISABLE KEYS */;
INSERT INTO `vendedores` (`CODIGO_VENDEDOR`, `NOMBRE_VENDEDOR`) VALUES
	(1, 'Esteban Ramos'),
	(2, 'Erick Brol'),
	(3, 'Nery Cruz'),
	(4, 'Estuardo Ventur');
/*!40000 ALTER TABLE `vendedores` ENABLE KEYS */;


-- Volcando estructura para vista facturacion.qryclientes
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `qryclientes`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `qryclientes` AS SELECT 
	clientes.CODIGO_CLIENTE, 
	clientes.NOMBRE_CLIENTE,
	vendedores.CODIGO_VENDEDOR,
	vendedores.NOMBRE_VENDEDOR
FROM clientes
inner join vendedores on clientes.CODIGO_VENDEDOR=vendedores.CODIGO_VENDEDOR ;


-- Volcando estructura para vista facturacion.qrydetallefactura
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `qrydetallefactura`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `qrydetallefactura` AS SELECT 
	detalle.CORRELATIVO,
	detalle.SERIE,
	detalle.NUMERO,
	productos.CODIGO_PRODUCTO,
	productos.DESCRIPCION,
	detalle.CANTIDAD,
	detalle.PRECIO,
	detalle.CANTIDAD*detalle.PRECIO AS SUBTOTAL
FROM detalle
inner join productos on detalle.CODIGO_PRODUCTO=productos.CODIGO_PRODUCTO ;


-- Volcando estructura para vista facturacion.qryresumenfacturas
-- Eliminando tabla temporal y crear estructura final de VIEW
DROP TABLE IF EXISTS `qryresumenfacturas`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `qryresumenfacturas` AS select 
	factura.SERIE AS Serie,
	factura.NUMERO AS Numero,
	factura.FECHA AS Fecha,
	clientes.NOMBRE_CLIENTE AS Cliente,
	sum(detalle.CANTIDAD * detalle.PRECIO) AS Total
from factura
inner join detalle on factura.SERIE=detalle.SERIE and factura.NUMERO=detalle.NUMERO
inner join clientes on factura.CODIGO_CLIENTE=clientes.CODIGO_CLIENTE
group by
	factura.SERIE,
	factura.NUMERO,
	factura.FECHA,
	clientes.NOMBRE_CLIENTE ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
