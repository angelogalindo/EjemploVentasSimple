﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <h1 class="page-header">
        RESUMEN DE VENTAS          
    </h1> 
    <telerik:RadGrid ID="grdVentasDelDia" runat="server" GroupPanelPosition="Top">
        <MasterTableView AutoGenerateColumns="False" DataKeyNames="Serie,Numero" AllowFilteringByColumn="True" ShowFooter="True">
            <Columns>
                <telerik:GridBoundColumn DataField="Serie" ReadOnly="True" HeaderText="Serie" SortExpression="Serie" UniqueName="Serie" DataType="System.Int32" FilterControlAltText="Filter Serie column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Numero" HeaderText="Numero" SortExpression="Numero" UniqueName="Numero" FilterControlAltText="Filter Numero column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" SortExpression="Fecha" UniqueName="Fecha" FilterControlAltText="Filter Fecha column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Cliente" HeaderText="Cliente" SortExpression="Cliente" UniqueName="Cliente" FilterControlAltText="Filter Cliente column"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Total" HeaderText="Total" SortExpression="Total" UniqueName="Total" DataType="System.Decimal" FilterControlAltText="Filter Total column" FooterText="Total: " Aggregate="SUM">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" />
                    <FooterStyle HorizontalAlign="Right" />
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
    </telerik:RadGrid>

</asp:Content>

