﻿
Partial Class Operacion_Clientes
    Inherits System.Web.UI.Page

    Private Parametros As MySqlParameterCollection = New MySqlCommand().Parameters
    Private WithEvents cls As New ControlDeDatos()

    Enum Estado
        SinEstado
        Nuevo
        Editando
    End Enum

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hdnEstado.Value = Estado.SinEstado
            Estados()
        End If
    End Sub

    Protected Sub cls_sqlError(DescripcionError As String) Handles cls.sqlError
        lblMensaje.Text = DescripcionError
    End Sub

    Private Sub Estados()
        If hdnEstado.Value = Estado.SinEstado Then
            cmdNuevo.Enabled = True
            cmdGuardar.Enabled = False
            cmdEliminar.Enabled = False
            cmdCancelar.Enabled = False
        ElseIf hdnEstado.Value = Estado.Nuevo Then
            cmdNuevo.Enabled = False
            cmdGuardar.Enabled = True
            cmdEliminar.Enabled = False
            cmdCancelar.Enabled = True
        ElseIf hdnEstado.Value = Estado.Editando Then
            cmdNuevo.Enabled = False
            cmdGuardar.Enabled = True
            cmdEliminar.Enabled = True
            cmdCancelar.Enabled = True
        End If
    End Sub

    Protected Sub cmdNuevo_Click(sender As Object, e As EventArgs) Handles cmdNuevo.Click
        hdnEstado.Value = Estado.Nuevo
        Estados()
    End Sub

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        If Page.IsValid Then
            lblMensaje.Text = ""
            If txtCliente.Text <> "" Then

                If hdnEstado.Value = Estado.Nuevo Then
                    Parametros.Add("@CODIGO_CLIENTE", MySqlDbType.Int24).Value = cls.Max("clientes", "CODIGO_CLIENTE")
                Else
                    Parametros.Add("@CODIGO_CLIENTE", MySqlDbType.Int24).Value = txtCodigoCliente.Text
                End If

                Parametros.Add("@NOMBRE_CLIENTE", MySqlDbType.VarChar).Value = txtCliente.Text
                Parametros.Add("@CODIGO_VENDEDOR", MySqlDbType.Int24).Value = cboVendedor.SelectedValue

                If hdnEstado.Value = Estado.Nuevo Then
                    If cls.Insertar("clientes", Parametros) Then
                        Response.Redirect(ResolveClientUrl("Clientes.aspx"), True)
                    End If
                ElseIf hdnEstado.Value = Estado.Editando Then
                    If cls.Actualizar("clientes", "CODIGO_CLIENTE", txtCodigoCliente.Text, Parametros) Then
                        Response.Redirect(ResolveClientUrl("Clientes.aspx"), True)
                    End If
                End If

                Parametros.Clear()

            Else
                lblMensaje.Text = "Debe ingresar un nombre para el cliente"
            End If

        End If
    End Sub

    Protected Sub cmdEliminar_Click(sender As Object, e As EventArgs) Handles cmdEliminar.Click
        If Page.IsValid Then
            If txtCodigoCliente.Text <> "" Then
                cls.EliminarDatos("clientes", "CODIGO_CLIENTE", txtCodigoCliente.Text)
                Response.Redirect(ResolveClientUrl("Clientes.aspx"), True)
            End If
        End If
    End Sub

    Protected Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Response.Redirect(ResolveClientUrl("Clientes.aspx"), True)
    End Sub

    Protected Sub grdClientes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdClientes.SelectedIndexChanged
        txtCodigoCliente.Text = grdClientes.MasterTableView.DataKeyValues(grdClientes.SelectedIndexes(0)).Item(grdClientes.MasterTableView.DataKeyNames(0))
        txtCliente.Text = grdClientes.MasterTableView.DataKeyValues(grdClientes.SelectedIndexes(0)).Item(grdClientes.MasterTableView.DataKeyNames(1))
        cboVendedor.SelectedValue = grdClientes.MasterTableView.DataKeyValues(grdClientes.SelectedIndexes(0)).Item(grdClientes.MasterTableView.DataKeyNames(2))
        hdnEstado.Value = Estado.Editando
        Estados()
    End Sub

End Class
