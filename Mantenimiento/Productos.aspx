﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Productos.aspx.vb" Inherits="Operacion_Productos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="page-header">
        Productos
        <br />   
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>         
        <asp:HiddenField ID="hdnEstado" runat="server" />
    </h1> 
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"> 
            <label>Codigo Producto</label>
            <asp:TextBox ID="txtCodigoProducto" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
            <label>Producto</label>
            <asp:TextBox ID="txtProducto" CssClass="form-control" MaxLength="15" runat="server"></asp:TextBox>
            <label>Producto Padre</label>
            <asp:TextBox ID="txtProductoPadre" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="row">
    <div class="col-lg-12"> 
            <br />
            <asp:Button ID="cmdNuevo" CssClass="btn btn-success"  runat="server" Text="Nuevo" />
            <asp:Button ID="cmdGuardar" CssClass="btn btn-primary"  runat="server" Text="Guardar" />
            <asp:Button ID="cmdEliminar" CssClass="btn btn-danger"  runat="server" Text="Eliminar" />
            <asp:Button ID="cmdCancelar" CssClass="btn btn-warning"  runat="server" Text="Cancelar" />
        </div>
    </div>
    <br /><br />
    <div class="row">
        <div class="col-lg-12">
            <telerik:RadGrid ID="grdProductos" runat="server" DataSourceID="sqlProductos" GroupPanelPosition="Top">
                <MasterTableView DataSourceID="sqlProductos" AutoGenerateColumns="False" DataKeyNames="CODIGO_PRODUCTO,DESCRIPCION,PRODUCTO_PADRE">
                    <Columns>
                        <telerik:GridButtonColumn CommandName="Select" Text="Seleccionar" FilterControlAltText="Filter column column" UniqueName="column"></telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="CODIGO_PRODUCTO" ReadOnly="True" HeaderText="Codigo Producto" SortExpression="CODIGO_PRODUCTO" UniqueName="CODIGO_PRODUCTO" DataType="System.Int32" FilterControlAltText="Filter CODIGO_PRODUCTO column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="DESCRIPCION" HeaderText="Descripcion" SortExpression="DESCRIPCION" UniqueName="DESCRIPCION" FilterControlAltText="Filter DESCRIPCION column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PRODUCTO_PADRE" HeaderText="Producto Padre" SortExpression="PRODUCTO_PADRE" UniqueName="PRODUCTO_PADRE" DataType="System.Int32" FilterControlAltText="Filter PRODUCTO_PADRE column"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>

            <asp:SqlDataSource runat="server" ID="sqlProductos" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="select CODIGO_PRODUCTO,DESCRIPCION,PRODUCTO_PADRE from productos"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>

