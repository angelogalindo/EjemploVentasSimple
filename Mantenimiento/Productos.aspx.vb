﻿
Partial Class Operacion_Productos
    Inherits System.Web.UI.Page

    Private Parametros As MySqlParameterCollection = New MySqlCommand().Parameters
    Private WithEvents cls As New ControlDeDatos()

    Enum Estado
        SinEstado
        Nuevo
        Editando
    End Enum

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hdnEstado.Value = Estado.SinEstado
            Estados()
        End If
    End Sub

    Protected Sub cmdNuevo_Click(sender As Object, e As EventArgs) Handles cmdNuevo.Click
        hdnEstado.Value = Estado.Nuevo
        Estados()
    End Sub

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        If Page.IsValid Then
            lblMensaje.Text = ""
            If txtProducto.Text <> "" And txtProductoPadre.Text <> "" Then

                If hdnEstado.Value = Estado.Nuevo Then
                    Parametros.Add("@CODIGO_PRODUCTO", MySqlDbType.Int24).Value = cls.Max("productos", "CODIGO_PRODUCTO")
                Else
                    Parametros.Add("@CODIGO_PRODUCTO", MySqlDbType.Int24).Value = txtCodigoProducto.Text
                End If

                Parametros.Add("@DESCRIPCION", MySqlDbType.VarChar).Value = txtProducto.Text
                Parametros.Add("@PRODUCTO_PADRE", MySqlDbType.Int24).Value = txtProductoPadre.Text

                If hdnEstado.Value = Estado.Nuevo Then
                    If cls.Insertar("productos", Parametros) Then
                        Response.Redirect(ResolveClientUrl("Productos.aspx"), True)
                    End If
                ElseIf hdnEstado.Value = Estado.Editando Then
                    If cls.Actualizar("productos", "CODIGO_PRODUCTO", txtCodigoProducto.Text, Parametros) Then
                        Response.Redirect(ResolveClientUrl("Productos.aspx"), True)
                    End If
                End If

                Parametros.Clear()


            Else
                lblMensaje.Text = "Debe ingresar una descripcion para el producto y un producto padre"
            End If

        End If
    End Sub

    Protected Sub cmdEliminar_Click(sender As Object, e As EventArgs) Handles cmdEliminar.Click
        If Page.IsValid Then
            If txtCodigoProducto.Text <> "" Then
                cls.EliminarDatos("productos", "CODIGO_PRODUCTO", txtCodigoProducto.Text)
                Response.Redirect(ResolveClientUrl("Productos.aspx"), True)
            End If
        End If
    End Sub

    Protected Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Response.Redirect(ResolveClientUrl("Productos.aspx"), True)
    End Sub



    Protected Sub cls_sqlError(DescripcionError As String) Handles cls.sqlError
        lblMensaje.Text = DescripcionError
    End Sub

    Private Sub Estados()
        If hdnEstado.Value = Estado.SinEstado Then
            cmdNuevo.Enabled = True
            cmdGuardar.Enabled = False
            cmdEliminar.Enabled = False
            cmdCancelar.Enabled = False
        ElseIf hdnEstado.Value = Estado.Nuevo Then
            cmdNuevo.Enabled = False
            cmdGuardar.Enabled = True
            cmdEliminar.Enabled = False
            cmdCancelar.Enabled = True
        ElseIf hdnEstado.Value = Estado.Editando Then
            cmdNuevo.Enabled = False
            cmdGuardar.Enabled = True
            cmdEliminar.Enabled = True
            cmdCancelar.Enabled = True
        End If
    End Sub

    Protected Sub grdProductos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdProductos.SelectedIndexChanged
        txtCodigoProducto.Text = grdProductos.MasterTableView.DataKeyValues(grdProductos.SelectedIndexes(0)).Item(grdProductos.MasterTableView.DataKeyNames(0))
        txtProducto.Text = grdProductos.MasterTableView.DataKeyValues(grdProductos.SelectedIndexes(0)).Item(grdProductos.MasterTableView.DataKeyNames(1))
        txtProductoPadre.Text = grdProductos.MasterTableView.DataKeyValues(grdProductos.SelectedIndexes(0)).Item(grdProductos.MasterTableView.DataKeyNames(2))
        hdnEstado.Value = Estado.Editando
        Estados()
    End Sub
End Class
