﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Clientes.aspx.vb" Inherits="Operacion_Clientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="page-header">
        Clientes  
        <br />   
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>         
        <asp:HiddenField ID="hdnEstado" runat="server" />
    </h1> 
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"> 
            <label>Codigo Cliente</label>
            <asp:TextBox ID="txtCodigoCliente" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
            <label>Cliente</label>
            <asp:TextBox ID="txtCliente" CssClass="form-control" MaxLength="15" runat="server"></asp:TextBox>
            <label>Vendedor</label>            
            <asp:DropDownList ID="cboVendedor" CssClass="form-control" runat="server" DataSourceID="sqlVendedores" DataTextField="NOMBRE_VENDEDOR" DataValueField="CODIGO_VENDEDOR"></asp:DropDownList>
            <asp:SqlDataSource runat="server" ID="sqlVendedores" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="SELECT 0 as CODIGO_VENDEDOR, 'Seleccione vendedor' NOMBRE_VENDEDOR
UNION
SELECT CODIGO_VENDEDOR,NOMBRE_VENDEDOR FROM vendedores"></asp:SqlDataSource>
   
        </div>
    </div>
    <div class="row">
    <div class="col-lg-12"> 
            <br />
            <asp:Button ID="cmdNuevo" CssClass="btn btn-success"  runat="server" Text="Nuevo" />
            <asp:Button ID="cmdGuardar" CssClass="btn btn-primary"  runat="server" Text="Guardar" />
            <asp:Button ID="cmdEliminar" CssClass="btn btn-danger"  runat="server" Text="Eliminar" />
            <asp:Button ID="cmdCancelar" CssClass="btn btn-warning"  runat="server" Text="Cancelar" />
        </div>
    </div>
    <br /><br />
    <div class="row">
        <div class="col-lg-12">
            <telerik:RadGrid ID="grdClientes" runat="server" DataSourceID="sqlClientes">
                <MasterTableView DataKeyNames="CODIGO_CLIENTE,NOMBRE_VENDEDOR,CODIGO_VENDEDOR" DataSourceID="sqlClientes" AutoGenerateColumns="False">
                    <Columns>
                        <telerik:GridButtonColumn CommandName="Select" Text="Seleccionar" FilterControlAltText="Filter column column" UniqueName="column"></telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="CODIGO_CLIENTE" ReadOnly="True" HeaderText="Codigo Cliente" SortExpression="CODIGO_CLIENTE" UniqueName="CODIGO_CLIENTE" DataType="System.Int32" FilterControlAltText="Filter CODIGO_CLIENTE column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="NOMBRE_CLIENTE" HeaderText="Nombre Cliente" SortExpression="NOMBRE_CLIENTE" UniqueName="NOMBRE_CLIENTE" FilterControlAltText="Filter NOMBRE_CLIENTE column"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CODIGO_VENDEDOR" ReadOnly="True" HeaderText="CODIGO_VENDEDOR" SortExpression="CODIGO_VENDEDOR" UniqueName="CODIGO_VENDEDOR" DataType="System.Int32" FilterControlAltText="Filter CODIGO_VENDEDOR column" Display="false"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="NOMBRE_VENDEDOR" HeaderText="Nombre Vendedor" SortExpression="NOMBRE_VENDEDOR" UniqueName="NOMBRE_VENDEDOR" FilterControlAltText="Filter NOMBRE_VENDEDOR column"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>

            <asp:SqlDataSource runat="server" ID="sqlClientes" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="select CODIGO_CLIENTE,NOMBRE_CLIENTE,CODIGO_VENDEDOR,NOMBRE_VENDEDOR from qryclientes ORDER BY CODIGO_CLIENTE "></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>

