﻿
Partial Class Operacion_Vendedores
    Inherits System.Web.UI.Page

    Private Parametros As MySqlParameterCollection = New MySqlCommand().Parameters
    Private WithEvents cls As New ControlDeDatos()

    Enum Estado
        SinEstado
        Nuevo
        Editando
    End Enum

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            hdnEstado.Value = Estado.SinEstado
            Estados()
        End If
    End Sub

    Protected Sub cmdNuevo_Click(sender As Object, e As EventArgs) Handles cmdNuevo.Click
        hdnEstado.Value = Estado.Nuevo
        Estados()
    End Sub

    Protected Sub cmdGuardar_Click(sender As Object, e As EventArgs) Handles cmdGuardar.Click
        If Page.IsValid Then
            lblMensaje.Text = ""
            If txtVendedor.Text <> "" Then

                If hdnEstado.Value = Estado.Nuevo Then
                    Parametros.Add("@CODIGO_VENDEDOR", MySqlDbType.Int24).Value = cls.Max("vendedores", "CODIGO_VENDEDOR")
                Else
                    Parametros.Add("@CODIGO_VENDEDOR", MySqlDbType.Int24).Value = txtCodigoVendedor.Text
                End If

                Parametros.Add("@NOMBRE_VENDEDOR", MySqlDbType.VarChar).Value = txtVendedor.Text

                If hdnEstado.Value = Estado.Nuevo Then
                    If cls.Insertar("vendedores", Parametros) Then
                        Response.Redirect(ResolveClientUrl("Vendedores.aspx"), True)
                    End If
                ElseIf hdnEstado.Value = Estado.Editando Then
                    If cls.Actualizar("vendedores", "CODIGO_VENDEDOR", txtCodigoVendedor.Text, Parametros) Then
                        Response.Redirect(ResolveClientUrl("Vendedores.aspx"), True)
                    End If
                End If

                Parametros.Clear()


            Else
                lblMensaje.Text = "Debe ingresar una descripcion vendedor"
            End If

        End If
    End Sub

    Protected Sub cmdEliminar_Click(sender As Object, e As EventArgs) Handles cmdEliminar.Click
        If Page.IsValid Then
            If txtCodigoVendedor.Text <> "" Then
                cls.EliminarDatos("vendedores", "CODIGO_VENDEDOR", txtCodigoVendedor.Text)
                Response.Redirect(ResolveClientUrl("Vendedores.aspx"), True)
            End If
        End If
    End Sub

    Protected Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Response.Redirect(ResolveClientUrl("Vendedores.aspx"), True)
    End Sub



    Protected Sub cls_sqlError(DescripcionError As String) Handles cls.sqlError
        lblMensaje.Text = DescripcionError
    End Sub

    Private Sub Estados()
        If hdnEstado.Value = Estado.SinEstado Then
            cmdNuevo.Enabled = True
            cmdGuardar.Enabled = False
            cmdEliminar.Enabled = False
            cmdCancelar.Enabled = False
        ElseIf hdnEstado.Value = Estado.Nuevo Then
            cmdNuevo.Enabled = False
            cmdGuardar.Enabled = True
            cmdEliminar.Enabled = False
            cmdCancelar.Enabled = True
        ElseIf hdnEstado.Value = Estado.Editando Then
            cmdNuevo.Enabled = False
            cmdGuardar.Enabled = True
            cmdEliminar.Enabled = True
            cmdCancelar.Enabled = True
        End If
    End Sub

    Protected Sub grdVendedores_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grdVendedores.SelectedIndexChanged
        txtCodigoVendedor.Text = grdVendedores.MasterTableView.DataKeyValues(grdVendedores.SelectedIndexes(0)).Item(grdVendedores.MasterTableView.DataKeyNames(0))
        txtVendedor.Text = grdVendedores.MasterTableView.DataKeyValues(grdVendedores.SelectedIndexes(0)).Item(grdVendedores.MasterTableView.DataKeyNames(1))
        hdnEstado.Value = Estado.Editando
        Estados()
    End Sub
End Class
