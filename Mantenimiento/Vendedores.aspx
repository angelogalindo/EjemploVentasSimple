﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Vendedores.aspx.vb" Inherits="Operacion_Vendedores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="page-header">
        Vendedores  
        <br />   
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>         
        <asp:HiddenField ID="hdnEstado" runat="server" />
    </h1> 
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6"> 
            <label>Codigo Vendedor</label>
            <asp:TextBox ID="txtCodigoVendedor" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
            <label>Vendedor</label>
            <asp:TextBox ID="txtVendedor" CssClass="form-control" MaxLength="15" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="row">
    <div class="col-lg-12"> 
            <br />
            <asp:Button ID="cmdNuevo" CssClass="btn btn-success"  runat="server" Text="Nuevo" />
            <asp:Button ID="cmdGuardar" CssClass="btn btn-primary"  runat="server" Text="Guardar" />
            <asp:Button ID="cmdEliminar" CssClass="btn btn-danger"  runat="server" Text="Eliminar" />
            <asp:Button ID="cmdCancelar" CssClass="btn btn-warning"  runat="server" Text="Cancelar" />
        </div>
    </div>
    <br /><br />
    <div class="row">
        <div class="col-lg-12">
            <telerik:RadGrid ID="grdVendedores" runat="server" DataSourceID="sqlVendedores" GroupPanelPosition="Top">
                <MasterTableView DataKeyNames="CODIGO_VENDEDOR,NOMBRE_VENDEDOR" DataSourceID="sqlVendedores" AutoGenerateColumns="False">
                    <Columns>
                        <telerik:GridButtonColumn CommandName="Select" Text="Seleccionar" FilterControlAltText="Filter column column" UniqueName="column"></telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="CODIGO_VENDEDOR" ReadOnly="True" HeaderText="Codigo Vendedor" SortExpression="CODIGO_VENDEDOR" UniqueName="CODIGO_VENDEDOR" DataType="System.Int32" FilterControlAltText="Filter CODIGO_VENDEDOR column"></telerik:GridBoundColumn>                        
                        <telerik:GridBoundColumn DataField="NOMBRE_VENDEDOR" HeaderText="Nombre Vendedor" SortExpression="NOMBRE_VENDEDOR" UniqueName="NOMBRE_VENDEDOR" FilterControlAltText="Filter NOMBRE_VENDEDOR column"></telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>

            <asp:SqlDataSource runat="server" ID="sqlVendedores" ConnectionString='<%$ ConnectionStrings:CN %>' ProviderName='<%$ ConnectionStrings:CN.ProviderName %>' SelectCommand="select CODIGO_VENDEDOR, NOMBRE_VENDEDOR from vendedores"></asp:SqlDataSource>
        </div>
    </div>
</asp:Content>

